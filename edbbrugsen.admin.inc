<?php

/**
 * @file
 * EDB-BRUGSEN module admin settings.
 */

/**
 * Return the EDB-BRUGS global settings form.
 */
function edbbrugsen_admin_settings() {

  $form['edbbrugsen_username'] = array(
    '#type' => 'textfield',
    '#title' => t('EDB-BRUGS username'),
    '#required' => TRUE,
    '#default_value' => variable_get('edbbrugsen_username', ''),
    '#description' => t('The username for EDB-BRUGS. Get it by contacting !link.', array('!termslink' => l(t('EDB-BRUGS support'), 'https://edb-brugs.dk'))),
  );

  $form['edbbrugsen_password'] = array(
    '#type' => 'textfield',
    '#title' => t('EDB-BRUGS Password'),
    '#required' => TRUE,
    '#default_value' => variable_get('edbbrugsen_password', ''),
    '#description' => t('Password for EDB-BRUGS.'),
  );

  $form['edbbrugsen_schoolcode'] = array(
    '#type' => 'textfield',
    '#title' => t('EDB-BRUGS School code'),
    '#required' => TRUE,
    '#default_value' => variable_get('edbbrugsen_schoolcode', ''),
    '#description' => t('Schoolcode for EDB-BRUGS.'),
  );

  return system_settings_form($form);
}
