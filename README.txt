EDB-BRUGSEN
==

Integration between Drupal and the Danish service provider <a href="http://edb-brugs.dk">EDB-BRUGS</a> which has a program for registering students and courses under <a href="http://edb-brugs.dk">De frie skoler</a>.

<h3>Under active development</h3>

This module is being developed right now. A lot of stuff is unfinished, and for now it is restricted for the use case of <a href="http://vies.dk">Vejle Idrætsefterskole</a> and <a href="http://vih.dk">Vejle Idrætshøjskole</a>.

However, we are interested in having the code abstracted so it can work as integration between different Drupal scenarios and EDB-BRUGS. Any help appreciated.

<h3>Requirements</h3>

<ul>
  <li><a href="http://drupal.org/project/libraries">libraries</a>
  <li><a href="https://github.com/vih/edbbrugsen-sdk">EDB-BRUGS PHP sdk</a>
</ul>

<h3>Sponsors</h3>

<ul>
  <li><a href="http://vies.dk">Vejle Idrætsefterskole</a></li>
  <li><a href="http://vih.dk">Vejle Idrætshøjskole</a></li>
</ul>